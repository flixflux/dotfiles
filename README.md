# dotfiles

**!! This is a Work in Progress project !!**

* install 
```
git clone --recursive https://gitlab.com/flixflux/dotfiles.git
```

* Checkout submudules
```
git submodule init && git submodule update
```

* (edit?) and run `./run.sh` to create the necessary link in ~
   or manually link and...

* Install tools:
```
pacman -S zsh screen vim fd fzf
```

* Install linter:
```
pacman -S shellcheck yamllint mypy
```

Todo's

* integrate [zprezto](https://github.com/sorin-ionescu/prezto)
