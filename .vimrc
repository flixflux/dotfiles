" ------ basics -------
set mouse=
set encoding=utf-8
set nonumber
syn on
filetype plugin on

" Set fzf runtime
set rtp+=/usr/bin/fzf

" spellchecking
" setlocal spell spelllang=en_us
set spell spelllang=de
set nospell

" Search related settings
set incsearch
set nohlsearch

" splitscren defaults & navigations
set splitbelow
set splitright
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

if &diff
    colorscheme blue
endif


" ------ tab-stuff ------
set tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab

" make uses real tabs
au FileType make set noexpandtab

" make Python follow PEP8 ( http://www.python.org/dev/peps/pep-0008/ )
au FileType python set softtabstop=4 tabstop=4 shiftwidth=4 textwidth=79


" ------ plugins -------
packloadall
silent! helptags ALL

" NertTree open when no file specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Close window if last remaining window is NerdTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" NERDTree configuration
let NERDTreeIgnore=['\.pyc$', '\.rbc$', '\~$']
map <Leader>n :NERDTreeToggle<CR>
nmap <Leader>r :NERDTreeFocus<cr>R<c-w><c-p>

let NERDTreeShowHidden=1

" liteline shows mode
set noshowmode
set laststatus=2
"let g:lightline = {'component_function': {'gitbranch': 'fugitive#head'}}

" testing here please:

map <Leader>w :%w !wl-copy<CR>

"Use TAB to complete when typing words, else inserts TABs as usual.
"Uses dictionary and source files to find matching words to complete.

"See help completion for source,
"Note: usual completion is on <C-n> but more trouble to press all the time.
"Never type the same word twice and maybe learn a new spellings!
"Use the Linux dictionary when spelling is in doubt.
"Window users can copy the file to their machine.
function! Tab_Or_Complete()
  if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
    return "\<C-N>"
  else
    return "\<Tab>"
  endif
endfunction
:inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>
:set dictionary="/data/pics/test/vim.dict"

