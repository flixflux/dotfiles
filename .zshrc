#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...

# keybindings

# vi stuff 
bindkey -v
export KEYTIMEOUT=1

#bindkey '^R' history-incremental-search-backward

bindkey -M vicmd '/' history-incremental-pattern-search-backward
bindkey -M vicmd '?' history-incremental-pattern-search-forward
# set up for insert mode too
bindkey -M viins '^R' history-incremental-pattern-search-backward
bindkey -M viins '^F' history-incremental-pattern-search-forward

# get git of 'execute' mode
bindkey -a -r ':'

# gpg-agent ssh setup
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent

export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null

# shell prompt nice and simple ;)
autoload -Uz promptinit
promptinit
prompt minimal

# azure-cli autocompletion
autoload -U +X bashcompinit && bashcompinit
source /usr/share/bash-completion/completions/az

# kubectl autocompletion
source <(kubectl completion zsh)
source <(helm completion zsh)

# glcoud completion
source /opt/google-cloud-sdk/path.zsh.inc 
source /opt/google-cloud-sdk/completion.zsh.inc



# Aliases
alias aspellde="aspell -c --language-tag=de /tmp/trash"
alias aspellen="aspell -c /tmp/trash"
alias vimspellde='vim -c "set nonumber" -c "set spell spelllang=de"'
alias vimspellen='vim -c "set nonumber" -c "set spell spelllang=en"'

alias ll="ls -ltr"
alias df='df -h'
alias mtr='mtr --curses'
alias vi='vim'
alias k='kubectl'
alias qrenc2asci='qrencode -t ansiutf8'
alias ttermite="termite -c $HOME/.config/termite/transp.config"
alias refresh-gcp-agent='gpg-connect-agent updatestartuptty /bye'
alias clip='head -n1 | wl-copy'
alias steam='flatpak run com.valvesoftware.Steam'

# environment variables
export NETHACKOPTIONS="color,rest_on_space,autodig,autoquiver"
export MPD_HOST=yipyip
#export BROWSER=`which firefox`
export VISUAL=vim
export EDITOR=vim
export LESS='-M -r'
#export PAGER="bat --style=auto"
#export PAGER="bat --plain"
export PAGER="less -r"
#export MANPAGER="bat -l man --style=plain"
export MANPAGER=`which less`
export LANG='en_US.utf8'
export BC_ENV_ARGS="$HOME/.bcargs"
##export PATH=$PATH:~/bin:~/AWS/bin:~/.local/bin
export PATH=$PATH:~/bin:~/AWS/bin:~/.local/bin:/opt/android-sdk-platform-tools/
# export CONSUL_HTTP_ADDR=127.0.0.1:8500
export RSYNC_PASSWORD=felix
export VAULT_ADDR="https://vault.parity-mgmt-vault.parity.io"
alias vt="vault login -address=$VAULT_ADDR -method=gcp -token-only  role="devops"  service_account="felixvogel@parity-mgmt-vault.iam.gserviceaccount.com" project="parity-mgmt-vault"  jwt_exp="10m" 'credentials=@/home/parity/.GCP/parity-mgmt-vault'"
export HELM_SECRETS_DRIVER=vals
export USE_GKE_GCLOUD_AUTH_PLUGIN=True


# whatever ...
echo UPDATESTARTUPTTY | gpg-connect-agent &> /dev/null

# FZF magic in zsh
if [[ -s "/usr/share/fzf/completion.zsh" ]]; then
  source "/usr/share/fzf/completion.zsh"
  export FZF_DEFAULT_COMMAND='fd --hidden -E .cache -E .git'
  export FZF_DEFAULT_OPTS="-m --no-mouse --cycle --color=dark --history=$HOME/.zhistory --bind 'alt-enter:execute(wl-copy {+})'"
fi
# BAT
# export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export BAT_THEME="TwoDark"
alias bat='bat -p -l yaml'

# PASS
## extention from here: https://github.com/ficoos/pass-fzf
export PASSWORD_STORE_ENABLE_EXTENSIONS=true
