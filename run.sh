#!/bin/bash

now=$(date +%s)
suffix=".$now-BAK"
filelist="
 .vimrc \
 .vim \
 .bcargs \
 .zshrc \
 .config/sway \
 .config/termite \
 .config/i3status \
 .config/ncmpc \
 .config/foot \
 .config/yamllint"

function install_dependencies {
  grep 'Arch' /etc/*issue && sudo \
  pacman -S zsh screen vim fd fzf shellcheck yamllint mypy
}
    
function save_and_link {
  file=$1
  if [ -e "$file" ] && [ ! -L "$file" ]
  then
    mv "${file}" "${file}${suffix}"
  fi
  ln -s ~/dotfiles/"$file" "$file"
}

install_dependencies

workingdir=$(pwd)
cd "$HOME" || exit

test -d ~/.config || mkdir ~/.config

for f in $filelist
do
  save_and_link "$f"
done

# check on host dependant sway config
if [ -e "$HOME/.config/sway/config.$(hostname)" ]
then
  test -L "$HOME/.config/sway/config" || \
  ln -s "$HOME/.config/sway/config.$(hostname)" ~/.config/sway/config
fi

cd "$workingdir" || exit

#vim -u NONE -c "helptags fugitive/doc" -c q
#vim -u NONE -c "helptags ~/.vim/pack/vendor/start/nerdtree/doc" -c q
